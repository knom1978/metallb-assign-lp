To assign a static ip on a private k8s
===
###### tags: `k8s` `metallb` `ip` `loadbalancer`
## Introduction
If you own a private k8s, you will find that k8s doens't provide the function of assign static ips. To cover this, metallb is designed.

## Procedure
To install metallb from yaml file
```
kubectl apply -f metallb.yaml
```
To create a config map, an ip pool is booked in this file. Be noticed that the pool should be in the same domain.
```
kubectl apply -f 2layer.yaml
```
To assign a static ip in the file
```
kubectl apply -f myapp.yaml
```

## Reference
https://metallb.universe.tf/usage/

https://blog.fleeto.us/post/intro-metallb/

https://juejin.im/post/5d9012bbe51d4577f4608b13

https://raw.githubusercontent.com/google/metallb/v0.7.1/manifests/metallb.yaml